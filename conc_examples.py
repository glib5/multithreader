"""
the easy way to parallel computing

https://docs.python.org/3/library/concurrent.futures.html

"""

from concurrent.futures import ProcessPoolExecutor as ProcessPool
from concurrent.futures import ThreadPoolExecutor as ThreadPool

from random import random
from time import sleep

# ---------------------------------------------

def fuzz():
    FUZZ = True
    if FUZZ:
        sleep(random())

# ---------------------------------------------

def __mysum(a, b):
    fuzz()
    return a + b

def __mysquare(x):
    fuzz()
    return x*x

# ---------------------------------------------

def _cube(x):
    """cpu intensive task"""
    fuzz()
    return x*x*x

def processes():
    inputs = list(range(5))
    with ProcessPool(max_workers=2) as pool:
        results = pool.map(_cube, inputs)
    for r in results:
        print(r)

# ---------------------------------------------

def _show(msg):
    """io intensive task"""
    fuzz()
    print(msg)

def threads():
    inputs = "A B C D E".split()
    with ThreadPool() as pool:
        results = pool.map(_show, inputs)
    for r in results:
        print(r)

# ---------------------------------------------

def different_tasks():
    with ProcessPool() as pool:
        task1 = pool.submit(__mysum, 1, 2)
        task2 = pool.submit(__mysquare, 9)
        res1 = task1.result()
        res2 = task2.result()
    print(res1, res2)

# ---------------------------------------------

if __name__=="__main__":

    processes()

    threads()

    different_tasks()

    input()

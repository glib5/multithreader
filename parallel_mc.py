
from concurrent.futures import ProcessPoolExecutor as ProcessPool
from time import perf_counter as pc

import numpy as np


def eu_call_mc(Trajs, strike, T, r):
    '''evaluates a vanilla call option'''
    kt = strike*np.exp(-r*T)
    kT = np.full(shape=Trajs.shape, fill_value=kt.reshape(len(kt), -1))
    payoff = np.where(Trajs - kT>.0, Trajs - kT, .0)
    return payoff


def mc_GBM(rand, reps, times, mu, sigma, dt, s0):
    '''returns N GMB trajs, as a matrix'''
    x = np.empty(shape=(times+1, reps))
    x[1:] = (mu-.5*sigma*sigma)*dt + rand.normal(size=(times, reps), scale=np.sqrt(dt)*sigma)
    np.add.accumulate(x[1:], out=x[1:], axis=0)
    np.exp(x[1:], out=x[1:])
    x[0] = s0
    x[1:] *= s0
    return x      


def MC(params):
    rand, reps, times, mu, sigma, dt, s0, strike, T, r, p_reps = params
    mc_call_ev = np.zeros(shape=(times+1), dtype=np.double)
    mc_call_err = np.zeros(shape=(times+1), dtype=np.double)
    for _ in range(reps//p_reps):
        J = mc_GBM(rand, p_reps, times, mu, sigma, dt, s0)
        a = eu_call_mc(J, strike, T, r)
        mc_call_ev += a.sum(axis=1)
        mc_call_err += (a*a).sum(axis=1)
    return (mc_call_ev, mc_call_err)


def parallel_mc(tot_reps, workers, reps, times, mu, sigma, dt, s0, strike, T, r, p_reps):    
    with ProcessPool(max_workers=workers) as pool:
        params = [tuple((np.random.RandomState(seed), reps, times, mu, sigma, dt, s0, strike, T, r, p_reps)) for seed in range(workers)]
        res = pool.map(MC, params)
    ES = np.zeros(shape=(times+1), dtype=np.double)
    ERR = np.zeros(shape=(times+1), dtype=np.double)
    for es, err in res:
        ES += es
        ERR += err
    ES /= tot_reps
    ERR = 3*np.sqrt((ERR/tot_reps - ES*ES)/tot_reps)
    return ES, ERR


def mc_plot(dt, ES, ERR):
    '''simple MC plotting routine'''
    import matplotlib.pyplot as plt
    ax = np.arange(len(ES))*dt
    plt.errorbar(x=ax, y=ES, yerr=ERR)
    plt.show()
    

def main():

    tot_reps = 1<<int(input("log_2 of reps? "))
    workers = 4
    if (tot_reps % workers) or (tot_reps < 1<<12):
        raise ValueError(f"'tot_reps' must be a multiple of 4 and >= 2**12, got {tot_reps}")
    reps = tot_reps // workers
    p_reps = 1<<10
    print(tot_reps, reps, p_reps, end="\n\n")
    times = 12
    mu = 0
    sigma = .3 
    dt = 1/4
    s0 = 4
    strike = 3.7 
    T = np.arange(times+1)*dt
    r = 0

    s = pc()
    ES, ERR = parallel_mc(tot_reps, workers, reps, times, mu, sigma, dt, s0, strike, T, r, p_reps)
    print(f"took {pc()-s} seconds")

    print("%4s +- %4s"%("E[S]", "ERR"))
    for es, err in zip(ES, ERR):
        print("%4.4f +- %4.4f"%(es, err))

    mc_plot(dt, ES, ERR)
    
    return 0

if __name__=="__main__":
    main()
    # input()

"""
we live in a state of sin

thread AFTER you fork
"""

from concurrent.futures import ProcessPoolExecutor as ProcessPool
from concurrent.futures import ThreadPoolExecutor as ThreadPool

from random import random
from time import sleep

# ---------------------------------------------

# for testing
def fuzz():
    FUZZ = True
    if FUZZ:
        sleep(random())

# ---------------------------------------------

# functions that will benefit from multithreading
def __mysum(params):
    a, b = params
    fuzz()
    return a + b

def __mysquare(x):
    fuzz()
    return x*x

# ---------------------------------------------

# jobs that use multithreading for the above functions
def job1(n):
    print("entering job 1")
    inputs = [(i, i+1) for i in range(n)]
    with ThreadPool() as pool:
        results = pool.map(__mysum, inputs)
    print("job 1 done")
    return list(results)

def job2(n):
    print("entering job 2")
    inputs = list(range(n))
    with ThreadPool() as pool:
        results = pool.map(__mysquare, inputs)
    print("job 2 done")
    return list(results)

# run multithreaded operations using different processes for each
def jobsplitter():
    N = 10
    print("start multiprocessing")
    with ProcessPool() as pool:
        task1 = pool.submit(job1, N)
        task2 = pool.submit(job2, N)
        res1 = task1.result()
        res2 = task2.result()
    print("finished multiprocessing")
    return (res1, res2)

# main
def main():
    res1, res2 = jobsplitter()
    print(res1)
    print(res2)

if __name__=="__main__":
    main()
    input()
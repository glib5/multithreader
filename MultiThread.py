from queue import Queue
from threading import Thread
from typing import List


class MultiThread:
    """
    ## MultiThread
    - object to run functions with multithreading.
    - Results may not be in the same order as inputs.

    ## example
    >>> inputs = [1, 2, 3]
    >>> mt = MultiThread(add_two_func, inputs)
    >>> x = mt.get_results()
    >>> x
    [4, 3, 5]
    """

    def __init__(self, func, inputs:List, maxsize:int=4):
        """
        This will initiate a multi thread processing of a function.
        Results can be retrieved via self.get_results()

        Args:
            func (callable): defines the function that should be called on the input data
            inputs (list | iterable): list of inputs for func
            maxsize (int, default=4): maximum number of simultaneous threads
        """

        # organize inputs
        self._q = Queue(maxsize=len(inputs))
        for i in inputs:
            self._q.put(i)
        # output list
        self.results = []
        # start threads
        for _ in range(maxsize):
            Thread(target=self._worker, args=[func], daemon=True).start()

    def _worker(self, func):
        """
        This is the worker function that retrieves input data from q

        Args:
            func (callable): function to be called on input data

        """

        while True:
            item = self._q.get()
            # function can take only 1 argument -> pass tuple of args!
            result = func(item)
            self.results.append(result)
            self._q.task_done()

    def get_results(self) -> List:
        """
        This returns a list of all results. 
        Results may not be in the same order as inputs.
        
        Return: 
            list of results
        """

        self._q.join()
        return self.results
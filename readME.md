# 

various examples with multithreading and multiprocessing

some nice classes and functions

## map_thread

like the built-in map, but with multithreading

- the mapped function can take only 1 parameter:
  - pass a namedtuple, dict or similar
- order of the output matches the one of the inputs

``` python
from map_thread import map_thread
# function to be multithreaded
def add_two(n:int) -> int:
    return n + 2
# inputs
numbers = list(range(10, 18))
# outputs
plus_2 = map_thread(add_two, numbers)
# order of output matches the inputs
for n, n2 in zip(numbers, plus_2):
    print(n, n2)
    

```

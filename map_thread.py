from queue import Queue
from threading import Thread
from typing import Any, Callable, Iterable, List


def _worker(func: Callable, q: Queue, output: List):
    """worker being multithreaded"""
    while True:
        index, param = q.get()
        output[index] = func(param)
        q.task_done()


def map_thread(func: Callable, inputs: Iterable, workers: int = 4) -> List:
    """
    executes the function 'func' on different threads
    for all the items in the input list. output has same
    order of input

    Args:
        func (callable): the function to be multithreaded
        can take only 1 argument
        inputs (List): list of inputs for the function func
        workers (int, default=4): number of simultaneous threads - maximum 32

    Returns:
        list of func results in the same order of inputs
        (like: list(map(func, inputs)) 
    """
    assert isinstance(func, Callable)
    assert isinstance(inputs, Iterable)
    assert isinstance(workers, int)
    assert (workers > 0)
    # output
    output: List[Any] = list(range(len(inputs)))
    # set up multithreading
    q = Queue()
    for index_param in enumerate(inputs):
        q.put(index_param)
    MAX_THREADS = 32
    # start threads
    for _ in range(min(workers, MAX_THREADS)):
        Thread(target=_worker, args=(func, q, output), daemon=True).start()
    # end all threads
    q.join()
    return output
